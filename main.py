import requests

api_url = "https://bm.morganperry.xyz/api/bookmarks/?format=json"
response = None
payload = None

with open('mykey') as f:
    key = f.read()
    headers = {'Authorization': 'Token {}'.format(key)}
    response = requests.get(api_url, headers=headers, data=payload)

bm_dict = response.json()

for i in bm_dict['results']:
    print(i['title'], '->', i['url'])
